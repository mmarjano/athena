# Hijing 

Author: Georgios Stavropoulos (George.Stavropoulos@cern.ch)

Documentation for the Hijing MC can be found here:
http://www-nsdth.lbl.gov/~xnwang/hijing/index.html

The Hijing interface documentation and explanations how to use Hijing in Athena framework is described here:
https://svnweb.cern.ch/trac/atlasoff/browser/Generators/Hijing_i/trunk/doc/Hijing.pdf
